<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * The Email service allows to send emails.
 * 
 * It use the symfony mailer component.
 * 
 * @author Côme Wasik <wasik.come@ŋmail.com>
 */
class EmailService
{
    /** @var MailerInterface $mailer A mailer interface */
    protected $mailer = null;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send a mail by simple mean.
     * 
     * You do not have to give many informations, it is cool for tests.
     * 
     * @param string $to Email receiver
     * @param string $subject Subject of the email
     * @param string $text Content of the email
     * 
     * @throws \RuntimeException The mail format is not correct
     */
    public function simpleSend(string $from, string $to, string $subject, string $text)
    {
        if (\filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
            throw new \RuntimeException('The email format of ' . $to . '  is not correct', 500);
        }

        $email = (new Email())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->text($text);

        $this->mailer->send($email);
    }
}
