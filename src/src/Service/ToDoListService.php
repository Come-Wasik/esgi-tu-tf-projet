<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\User;
use RuntimeException;

/**
 * The todolist service is simlifier object for users.
 * It allows to add items in user todolist.
 * 
 * @author Côme Wasik <wasik.come@ŋmail.com>
 */
class ToDoListService
{
    /** @var EmailService $mailer A mailer service */
    protected $mailer = null;

    /** @var string $adminEmail The email of the administrator */
    protected $adminEmail = null;

    public function __construct(EmailService $mailer, string $adminEmail)
    {
        $this->mailer = $mailer;
        $this->adminEmail = $adminEmail;
    }

    /**
     * Add an item to a user todolist.
     * 
     * @param User $user The user with the todolist
     * @param Item $item The item to add in the todolist
     */
    public function add(User $user, Item $item)
    {
        if (!$user->getTodolist()) {
            throw new RuntimeException('The user does not have a todolist', 500);
        }

        $user->getTodolist()->addItem($item);

        if ($user->getTodolist()->getItems()->count() == 8) {
            $this->mailer->simpleSend(
                $this->adminEmail,
                $user->getEmail(),
                'Your todolist is almost full',
                'As described in subject, your todolist have many items, 8 exactly, and it remains 2 places for adding items.'
            );
        }
        return $user;
    }
}
