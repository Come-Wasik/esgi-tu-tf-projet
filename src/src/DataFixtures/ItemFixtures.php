<?php

namespace App\DataFixtures;

use App\Entity\Item;
use APp\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var User $user The referenced user used by userFixtures */
        $user = $this->getReference(UserFixtures::USER_REFERENCE);

        // Create item and add it to the user's todolist
        $faker = \Faker\Factory::create('fr_FR');
        $item = new Item();
        $item = $item
            ->setName($faker->firstName)
            ->setContent('This is a simple content')
            ->setCreatedAt(new \DateTime('now'));

        $user->getTodolist()->addItem($item);

        // Persisting it in the database
        $manager->persist($user);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            TodolistFixtures::class,
        );
    }
}
