<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USER_REFERENCE = 'user';

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        $user = new User();

        $user = $user
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setEmail($faker->email)
            ->setPassword('adminpassword')
            ->setBirthday((new \DateTime('now'))->sub(new \DateInterval('P13Y')));

        $manager->persist($user);
        $manager->flush();

        $userInDb = $manager->getRepository(User::class)->findOneBy([
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname()
        ]);

        $this->addReference(self::USER_REFERENCE, $userInDb);
    }
}
