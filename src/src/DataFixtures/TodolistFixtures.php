<?php

namespace App\DataFixtures;

use App\Entity\Todolist;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TodolistFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var User $user The referenced user used by userFixtures */
        $user = $this->getReference(UserFixtures::USER_REFERENCE);

        // Create todolist and add it to the user
        $todolist = new Todolist();
        $user->setTodolist($todolist);

        // Persisting it in the database
        $manager->persist($user);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}
