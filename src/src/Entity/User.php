<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password; // 255 is a good lenght for a hashed password

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday;

    /**
     * @ORM\OneToOne(targetEntity=Todolist::class, mappedBy="utilisateur", cascade={"persist", "remove"})
     */
    private $todolist;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getTodolist(): ?Todolist
    {
        return $this->todolist;
    }

    public function setTodolist(Todolist $todolist): self
    {
        $this->todolist = $todolist;

        // set the owning side of the relation if necessary
        if ($todolist->getUtilisateur() !== $this) {
            $todolist->setUtilisateur($this);
        }

        return $this;
    }

    /**
     * Assert that the current user is valid
     * 
     * @throws \RuntimeException The password is not between 8 and 40 chars
     * @throws \RuntimeException The email is incorrect
     * @throws \RuntimeException The age is less than 13
     */
    public function checkValidity(): void
    {
        $errors = [];

        // Firstname verification
        if (empty($this->firstname)) {
            $errors[] = 'The firstname is empty';
        }

        // Lastname verification
        if (empty($this->lastname)) {
            $errors[] = 'The lastname is empty';
        }

        // Email verification
        if (\filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) {
            $errors[] = 'the email ' . $this->email . ' is incorrect';
        }

        // Password size verification
        if (strlen($this->password) < 8 || strlen($this->password) > 40) {
            $errors[] = 'the password must have a size between 8 and 40 characters';
        }

        // Minimum age verification
        if ((new \DateTime('now'))->diff($this->birthday)->y < 13) {
            $errors[] = 'the age of the user must be more than 13 years old';
        }

        if (!empty($errors)) {
            throw new \RuntimeException('There is/are error(s): ' . \implode('; ', $errors), 500);
        }
    }
}
