<?php

namespace App\Entity;

use App\Repository\TodolistRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TodolistRepository::class)
 */
class Todolist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="todolist", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="todolist", orphanRemoval=true, cascade={"persist"})
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        // False if the item is not unique OR the todolist have 10 items or more
        if ($this->items->contains($item)) {
            throw new \RuntimeException('You cannot add the item because it already exists', 500);
        }

        if ($this->items->count() >= 10) {
            throw new \RuntimeException('You cannot add the item because the maximum quantity is 10', 500);
        }

        if (is_null($item->getCreatedAt())) {
            throw new \InvalidArgumentException('You want to add an item which has no createdAt property', 500);
        }

        // False is there is a delay of less than 30 minutes between the current and the last item registered
        if ($this->items->count() > 0) {

            /** @var Datetime $newer Last dated item registered in the list */
            $newer = null;
            foreach ($this->items as $curItem) {
                if ($newer === null || $curItem->getCreatedAt() > $newer) {
                    $newer = $curItem->getCreatedAt();
                }
            }

            // True if there is a delay of less than 30 minutes between the current and the last item
            if ($item->getCreatedAt()->getTimestamp() - $newer->getTimestamp() < 60 * 30) {
                throw new \RuntimeException(
                    'You cannot add the item because the delay with the last item. Please wait : ' . ((60 * 30) - ($item->getCreatedAt()->getTimestamp() - $newer->getTimestamp())) . ' seconds',
                    500
                );
            }
        }

        $this->items[] = $item;
        $item->setTodolist($this);

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getTodolist() === $this) {
                $item->setTodolist(null);
            }
        }

        return $this;
    }
}
