<?php

namespace App\Controller\Api;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use App\Repository\UserRepository;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user", name="api_user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     * 
     * Show all users
     */
    public function index(UserRepository $repo): Response
    {
        $usersInArray = [];
        foreach ($repo->findAll() as $userInDb) {
            /** @var \App\Entity\User $user */

            // Firstly, we get the user data
            $user = [
                'id' => $userInDb->getId(),
                'firstname' => $userInDb->getFirstname(),
                'lastname' => $userInDb->getLastname(),
                'email' => $userInDb->getEmail(),
                'birthday' => $userInDb->getBirthday()->format('c'),
                'todolist' => [
                    'id' => $userInDb->getTodolist() ? $userInDb->getTodolist()->getId() : null,
                    'itemQuantity' => $userInDb->getTodolist() ? $userInDb->getTodolist()->getItems()->count() : null
                ]
            ];

            // Secondly, we get the user users
            $items = [];
            if ($userInDb->getTodolist() && $userInDb->getTodolist()->getItems()) {
                foreach ($userInDb->getTodolist()->getItems() as $itemInDb) {
                    /** @var \App\Entity\Item $item */
                    $items[] = [
                        'id' => $itemInDb->getId(),
                        'name' => $itemInDb->getName(),
                        'content' => $itemInDb->getContent(),
                        'created_at' => $itemInDb->getCreatedAt(),
                        'todolist_id' => $itemInDb->getTodolist()->getId(),
                        'user_id' => $itemInDb->getId()
                    ];
                }
            }
            // Finally, we register items in the user array
            $user['items'] = $items;
            $usersInArray[] = $user;
        }

        return $this->json($usersInArray);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     * 
     * Show an user
     */
    public function show(User $userInDb): Response
    {
        // Firstly, we get the user data
        $userInArray = [
            'firstname' => $userInDb->getFirstname(),
            'lastname' => $userInDb->getLastname(),
            'email' => $userInDb->getEmail(),
            'birthday' => $userInDb->getBirthday()->format('c'),
            'todolist' => [
                'id' => $userInDb->getTodolist() ? $userInDb->getTodolist()->getId() : null,
                'itemQuantity' => $userInDb->getTodolist() ? $userInDb->getTodolist()->getItems()->count() : null
            ],
        ];

        // Secondly, we get the user users
        $items = [];
        if ($userInDb->getTodolist() && $userInDb->getTodolist()->getItems()) {
            foreach ($userInDb->getTodolist()->getItems() as $itemInDb) {
                /** @var \App\Entity\user $user */
                $items[] = [
                    'id' => $itemInDb->getId(),
                    'name' => $itemInDb->getName(),
                    'content' => $itemInDb->getContent(),
                    'created_at' => $itemInDb->getCreatedAt(),
                    'todolist_id' => $itemInDb->getTodolist()->getId(),
                    'user_id' => $itemInDb->getId()
                ];
            }
        }

        // Finally, we register items in the user array
        $userInArray['items'] = $items;

        return $this->json($userInArray);
    }

    /**
     * @Route("/", name="new", methods={"POST"})
     * 
     * Add a user
     */
    public function new(RequestStack $stack): Response
    {
        $request = $stack->getCurrentRequest()->request;

        // Verify the construction of the userData array
        foreach (['firstname', 'lastname', 'email', 'password', 'birthday'] as $key) {
            if (!$request->has($key)) {
                throw new RuntimeException('User does not have the' . $key . ' property', 500);
            }
        }

        // Create an user from the userData
        $user = new User();
        $user
            ->setFirstname($request->get('firstname'))
            ->setLastname($request->get('lastname'))
            ->setEmail($request->get('email'))
            ->setPassword($request->get('password'))
            ->setBirthday(new \Datetime($request->get('birthday')));

        // If you want to set a todolist
        if ($request->has('todolist') && $request->get('todolist') === true) {
            $todolist = new Todolist();
            $user->setTodolist($todolist);
        }

        $user->checkValidity();

        // Fetch user from database
        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->flush();

        return $this->json('User added');
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * 
     * Delete a user
     */
    public function delete(User $user): Response
    {
        // Fetching user from the database
        $em = $this->getDoctrine()->getManager();

        // Removing user from the database
        $em->remove($user);
        $em->flush();

        return $this->json('User deleted');
    }

    /**
     * @Route("/{id}", name="update", methods={"UPDATE", "PATCH"})
     * 
     * Update a user
     */
    public function update(User $user, RequestStack $stack): Response
    {
        $em = $this->getDoctrine()->getManager();
        $request = $stack->getCurrentRequest()->query;

        // Todolist type verification
        if (
            $request->has('todolist') &&
            (!\is_int($request->all()['todolist']) && !\is_bool($request->all()['todolist']))
        ) {
            throw new RuntimeException('Try to update a todolist with a non bool or integer value', 500);
        }

        // Update a specific property in the item
        if ($request->has('firstname')) {
            $user = $user->setFirstname($request->get('firstname'));
        }
        if ($request->has('lastname')) {
            $user = $user->setLastname($request->get('lastname'));
        }
        if ($request->has('email')) {
            $user = $user->setEmail($request->get('email'));
        }
        if ($request->has('birthday')) {
            $user = $user->setBirthday(new \DateTime($request->get('birthday')));
        }
        if ($request->has('password')) {
            $user->setPassword($request->get('password'));
        }
        if ($request->has('todolist')) {
            // Add a new todolist if not exists
            if ($request->get('todolist') === true && !$user->getTodolist()) {
                $todolist = new Todolist();
                $user->setTodolist($todolist);

                // Change the todolist
            } elseif (\is_int($request->get('todolist'))) {
                $todolist = $em->getRepository(Todolist::class)->findOneBy(['id' => $request->get('todolist')]);

                if (!$todolist) {
                    throw new RuntimeException('Cannot fetch the ' . $request->get('todolist') . ' todolist id. It doesn\'t exists', 500);
                }

                $user->setTodolist($todolist);
            }
        }

        // Persist the update in the database
        $em->persist($user);
        $em->flush();

        return $this->json('User updated');
    }
}
