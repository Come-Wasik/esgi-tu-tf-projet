<?php

namespace App\Controller\Api;

use App\Entity\Item;
use App\Entity\User;
use App\Repository\ItemRepository;
use App\Repository\TodolistRepository;
use App\Repository\UserRepository;
use App\Service\EmailService;
use App\Service\ToDoListService;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/item", name="api_item_")
 */
class ItemController extends AbstractController
{
    /** @var string $adminEmail The admin email */
    protected $adminEmail = 'admin@domain.com';


    /**
     * @Route("/", name="index", methods={"GET"})
     * 
     * Show all items from a todolist (for a specific user)
     */
    public function index(ItemRepository $repo): Response
    {
        $items = [];
        foreach ($repo->findAll() as $item) {
            /** @var \App\Entity\Item $item */
            $items[] = [
                'id' => $item->getId(),
                'name' => $item->getName(),
                'content' => $item->getContent(),
                'created_at' => $item->getCreatedAt(),
                'todolist_id' => $item->getTodolist()->getId(),
                'user_id' => $item->getTodolist()->getUtilisateur()->getId()
            ];
        }
        return $this->json($items);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     * 
     * Show an item
     */
    public function show(Item $item): Response
    {
        $jsonedItem = [
            'name' => $item->getName(),
            'content' => $item->getContent(),
            'created_at' => $item->getCreatedAt(),
            'todolist_id' => $item->getTodolist()->getId(),
            'user_id' => $item->getTodolist()->getUtilisateur()->getId()
        ];
        return $this->json($jsonedItem);
    }

    /**
     * @Route("/", name="new", methods={"POST"})
     * 
     * Add an item into a todolist (for a specific user)
     */
    public function new(RequestStack $stack, EmailService $mailer): Response
    {
        $request = $stack->getCurrentRequest()->request;

        // Verify the existance of post arguments
        foreach (['userId', 'item'] as $key) {
            if (!$request->has($key)) {
                throw new RuntimeException('Missing ' . $key . ' argument', 500);
            }
        }

        // Params from request
        $userId = $request->get('userId');
        $itemData = $request->get('item');

        // Verify the construction of the itemData array
        if (!isset($itemData['name']) || !isset($itemData['content'])) {
            throw new RuntimeException('Item is not under a valid form', 500);
        }

        // Create an item from the itemData
        $item = new Item();
        $item
            ->setName($itemData['name'])
            ->setContent($itemData['content'])
            ->setCreatedAt(new \DateTime($itemData['date'] ?? 'now'));

        // Fetch user from database
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['id' => $userId]);

        // User existance verification
        if (!$user) {
            throw new RuntimeException('User ' . $userId . ' does not exists', 500);
        }

        /** @var User $user */
        if (!$user->getTodolist()) {
            throw new RuntimeException('This user doest not have a todolist. You cannot add any item.', 500);
        }

        $todolistService = new ToDoListService($mailer, $this->adminEmail);
        $user = $todolistService->add($user, $item);

        $em->persist($user);
        $em->flush();

        return $this->json('Item added');
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * 
     * Delete an item from a todolist (for a specific user)
     */
    public function delete(Item $item): Response
    {
        // Removing item from the database
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();

        return $this->json('Item deleted');
    }

    /**
     * @Route("/{id}", name="update", methods={"UPDATE", "PATCH"})
     * 
     * Delete an item from a todolist (for a specific user)
     */
    public function update(Item $item, RequestStack $stack): Response
    {
        $request = $stack->getCurrentRequest()->query;

        // Update an item from the itemData
        if ($request->has('item')) {
            $item = $item->setName($request->get('name'));
        }
        if ($request->has('content')) {
            $item = $item->setContent($request->get('content'));
        }

        // Update item in the database
        $em = $this->getDoctrine()->getManager();
        $em->persist($item);
        $em->flush();

        return $this->json('Item updated');
    }
}
