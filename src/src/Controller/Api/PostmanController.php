<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/test", name="api_user_")
 */
class PostmanController extends AbstractController
{
    /**
     * @Route("", name="new2", methods={"POST"})
     * 
     * Add a user
     */
    public function test(RequestStack $stack)
    {
        $request = $stack->getCurrentRequest()->request;

        return $this->json(['response' => $request]);
    }
}
