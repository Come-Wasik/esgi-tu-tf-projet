<?php

namespace App\Tests\Functional;

use App\Entity\Item;
use App\Entity\User;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ItemApiControllerTest extends WebTestCase
{
    /**
     * Test the route api_user_index
     * When it works
     */
    public function testItemIndexRoute()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_user_index', [], false);
        $crawler = $client->request('GET', $url, []);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_show
     * When it works
     */
    public function testItemShowRoute()
    {
        $client = static::createClient();

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Get users from the database
        $items = $em->getRepository(Item::class)->findAll();

        if (!$items) {
            throw new RuntimeException('No item was found. Please add a user of execute a fixture.', 500);
        }

        /** @var User $user A user in the database */
        $item = $items[\array_key_first($items)];

        $url = $client->getContainer()->get('router')->generate('api_item_show', ['id' => $item->getId()], false);
        $crawler = $client->request('GET', $url, []);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_show
     */
    public function testItemShowRouteDontFindAnObject()
    {
        $client = static::createClient();
        $url = $client->getContainer()->get('router')->generate('api_item_show', ['id' => 0], false);
        $crawler = $client->request('GET', $url, []);
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * Test the route api_item_new
     * When it works
     */
    public function testItemAddedRoute()
    {
        $client = static::createClient();

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Get users from the database
        $users = $em->getRepository(User::class)->findAll();

        if (!$users) {
            throw new RuntimeException('No user was found. Please add a user of execute a fixture.', 500);
        }

        $user = null;
        foreach ($users as $curUser) {
            /** @var User $user */
            if ($curUser->getTodolist() !== null) {
                $user = $curUser;
            }
        }
        if (!$user) {
            throw new RuntimeException('None users have a todolist', 500);
        }

        // New item attributes
        $faker = \Faker\Factory::create('fr_FR');
        $itemName = $faker->firstName();
        $itemContent = 'This is a default content';

        $url = $client->getContainer()->get('router')->generate('api_item_new', [], false);
        $crawler = $client->request('POST', $url, [
            'userId' => $user->getId(),
            'item' => [
                'name' => $itemName,
                'content' => $itemContent,
                'date' => (new \DateTime('now'))->add(new \DateInterval('PT30M'))->format('c') // Optionnal
            ]
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_new
     * Don't have userId
     */
    public function testItemAddedRouteDontHaveUserId()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_new', [], false);
        $crawler = $client->request('POST', $url, [
            'item' => [
                'name' => 'test',
                'content' => 'test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(500, 'Missing userId or item argument');
    }

    /**
     * Test the route api_item_new
     * Don't have item
     */
    public function testItemAddedRouteDontHaveItem()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_new', [], false);
        $crawler = $client->request('POST', $url, [
            'userId' => 1
        ]);

        $this->assertResponseStatusCodeSame(500, 'Missing userId or item argument');
    }

    /**
     * Test the route api_item_new
     * Item don't have name
     */
    public function testItemAddedRouteItemDontHaveName()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_new', [], false);
        $crawler = $client->request('POST', $url, [
            'userId' => 1,
            'item' => [
                // 'name' => $itemName,
                'content' => 'test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(500, 'Item is not under a valid form');
    }

    /**
     * Test the route api_item_new
     * Item don't have content
     */
    public function testItemAddedRouteItemDontHaveContent()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_new', [], false);
        $crawler = $client->request('POST', $url, [
            'userId' => 1,
            'item' => [
                'name' => 'test'
                // 'content' => 'test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(500, 'Item is not under a valid form');
    }

    /**
     * Test the route api_item_new
     * User does not exists
     */
    public function testItemAddedRouteUserDoesNotExists()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_new', [], false);
        $crawler = $client->request('POST', $url, [
            'userId' => 0,
            'item' => [
                'name' => 'test',
                'content' => 'test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(500, 'User 0 does not exists');
    }

    /**
     * Test the route api_item_delete
     * When it works
     */
    public function testItemDeleteRoute()
    {
        $client = static::createClient();

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Fetching item from the database
        $items = $em->getRepository(Item::class)->findAll();
        /** @var User $user A user in the database */
        $item = $items[\array_key_last($items)];

        if (!$item) {
            throw new RuntimeException('No item in the database');
        }

        $url = $client->getContainer()->get('router')->generate('api_item_delete', ['id' => 1], false);
        $crawler = $client->request('DELETE', $url, []);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_delete
     * Item does not exists
     */
    public function testItemDeleteRouteItemDoesNotExists()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_delete', ['id' => 0], false);
        $crawler = $client->request('DELETE', $url, []);
        $this->assertResponseStatusCodeSame(404, 'Item id 0 is supposed not existing');
    }

    /**
     * Test the route api_item_update
     * When it works
     */
    public function testItemUpdateRoute()
    {
        $client = static::createClient();

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Fetching item from the database
        $items = $em->getRepository(Item::class)->findAll();
        /** @var Item $item An item in the database */
        $item = $items[\array_key_first($items)];

        if (!$item) {
            throw new RuntimeException('Item does not exists');
        }

        // New item attributes
        $faker = \Faker\Factory::create('fr_FR');
        $itemName = $faker->firstName();
        $itemContent = $faker->text;

        $url = $client->getContainer()->get('router')->generate('api_item_update', ['id' => $item->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'name' => $itemName,
            'content' => $itemContent
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_update
     * Works when item does not have a name data
     */
    public function testItemUpdateRouteDontHaveItemWithNameData()
    {
        $client = static::createClient();

        // Fetch one item from database to have an item id
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');
        $items = $em->getRepository(Item::class)->findAll();
        /** @var Item $item An item in the database */
        $item = $items[\array_key_last($items)];

        $url = $client->getContainer()->get('router')->generate('api_item_update', ['id' => $item->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'content' => 'test'
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_update
     * Works when item does not have a content data
     */
    public function testItemUpdateRouteDontHaveItemWithContentData()
    {
        $client = static::createClient();

        // Fetch one item from database to have an item id
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');
        $items = $em->getRepository(Item::class)->findAll();
        /** @var Item $item An item in the database */
        $item = $items[\array_key_last($items)];

        $url = $client->getContainer()->get('router')->generate('api_item_update', ['id' => $item->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'name' => 'test'
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_item_update
     * Item does not exists
     */
    public function testItemUpdateRouteItemDoesNotExists()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_item_update', ['id' => 0], false);
        $crawler = $client->request('UPDATE', $url, []);
        $this->assertResponseStatusCodeSame(404, 'The item with id 0 is supposed to not existing');
    }
}
