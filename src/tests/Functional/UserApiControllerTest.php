<?php

namespace App\Tests\Functional;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use App\Service\CommandRunnerService;
use DateTime;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserApiControllerTest extends WebTestCase
{
    /**
     * Test the route api_user_index
     * When it works
     */
    public function testUserIndexRoute()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_user_index', [], false);
        $crawler = $client->request('GET', $url, []);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_user_show
     * When it works
     */
    public function testUserShowRoute()
    {
        $client = static::createClient();

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Get users from the database
        $users = $em->getRepository(User::class)->findAll();

        if (!$users) {
            throw new RuntimeException('No user was found. Please add a user of execute a fixture.', 500);
        }

        /** @var User $user A user in the database */
        $user = $users[\array_key_first($users)];

        $url = $client->getContainer()->get('router')->generate('api_user_show', ['id' => $user->getId()], false);
        $crawler = $client->request('GET', $url, []);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_user_show
     */
    public function testUserShowRouteDontFindAnObject()
    {
        $client = static::createClient();
        $url = $client->getContainer()->get('router')->generate('api_user_show', ['id' => 0], false);
        $crawler = $client->request('GET', $url, []);
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * Test the route api_user_new
     * When it works (create todolist version)
     */
    public function testUserAddedRouteCreateTodolist()
    {
        $client = static::createClient();

        // New item attributes
        $faker = \Faker\Factory::create('fr_FR');

        $url = $client->getContainer()->get('router')->generate('api_user_new', [], false);
        $crawler = $client->request('POST', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastName(),
            'email' => $faker->email(),
            'password' => $faker->password(8, 40),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'todolist' => true
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_user_new
     * When it works (don't create todolist version)
     */
    public function testUserAddedRouteDontCreateTodolist()
    {
        $client = static::createClient();

        // New item attributes
        $faker = \Faker\Factory::create('fr_FR');

        $url = $client->getContainer()->get('router')->generate('api_user_new', [], false);
        $crawler = $client->request('POST', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastName(),
            'email' => $faker->email(),
            'password' => $faker->password(8, 40),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'todolist' => false
        ]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_user_new
     * Don't have any user data
     */
    public function testUserAddedRouteDontAnyUserData()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_user_new', [], false);
        $userData = [
            'firstname' => 'test',
            'lastname' => 'test',
            'email' => 'test',
            'password' => 'test',
            'birthday' => 'test',
        ];

        // Delete each time one key
        foreach (['firstname', 'lastname', 'email', 'password', 'birthday'] as $key) {
            $curArray = $userData;
            unset($curArray[$key]);
            $crawler = $client->request('POST', $url, $curArray);
            $this->assertResponseStatusCodeSame(500, 'User does not require the' . $key . ' key');
        }
    }

    /**
     * Test the route api_user_delete
     * When it works
     */
    public function testItemDeleteRoute()
    {
        $client = static::createClient();

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Fetching item from the database
        $users = $em->getRepository(User::class)->findAll();
        /** @var User $user A user in the database */
        $user = $users[\array_key_last($users)];

        if (!$user) {
            throw new RuntimeException('No user exist in the database');
        }

        $url = $client->getContainer()->get('router')->generate('api_user_delete', ['id' => $user->getId()], false);
        $crawler = $client->request('DELETE', $url, []);
        $this->assertResponseIsSuccessful();
    }

    /**
     * Test the route api_user_delete
     * User does not exists
     */
    public function testItemDeleteRouteUserDoesNotExists()
    {
        $client = static::createClient();

        $url = $client->getContainer()->get('router')->generate('api_user_delete', ['id' => 0], false);
        $crawler = $client->request('DELETE', $url, []);
        $this->assertResponseStatusCodeSame(404, 'User 0 must not be found');
    }

    /**
     * Test the route api_user_update
     * When it works (without todolist before/ without todolist after)
     */
    public function testUserUpdateRouteWithoutTodoBerWithoutTodoAft()
    {
        $client = static::createClient();

        // Add new user
        $faker = \Faker\Factory::create('fr_FR');

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Create a user in the database
        $user = new User();
        $user = $user
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setEmail($faker->email)
            ->setPassword($faker->password(8, 40))
            ->setBirthday(new \DateTime((new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c')));

        $em->persist($user);
        $em->flush();

        // Update this user
        $url = $client->getContainer()->get('router')->generate('api_user_update', ['id' => $user->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastname(),
            'email' => $faker->email(),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'password' => $faker->password(8, 40)
        ]);
        $this->assertResponseIsSuccessful();

        $em->remove($user);
        $em->flush();
    }

    /**
     * Test the route api_user_update
     * When it works (without todolist before/ with todolist after)
     */
    public function testUserUpdateRouteWithoutTodoBefWithTodoAft()
    {
        $client = static::createClient();

        // Add new user
        $faker = \Faker\Factory::create('fr_FR');

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Create a user in the database
        $user = new User();
        $user = $user
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setEmail($faker->email)
            ->setPassword($faker->password(8, 40))
            ->setBirthday(new \DateTime((new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c')));

        $em->persist($user);
        $em->flush();

        // Update this user
        $url = $client->getContainer()->get('router')->generate('api_user_update', ['id' => $user->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastname(),
            'email' => $faker->email(),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'todolist' => true
        ]);
        $this->assertResponseIsSuccessful();

        $em->remove($user);
        $em->flush();
    }

    /**
     * Test the route api_user_update
     * When it works (with todolist before/ with todolist updated after)
     */
    public function testUserUpdateRouteWithTodoBefWithTodoUpdAft()
    {
        $client = static::createClient();

        // Add new user
        $faker = \Faker\Factory::create('fr_FR');

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Create a user in the database
        $user = new User();
        $user = $user
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setEmail($faker->email)
            ->setPassword($faker->password(8, 40))
            ->setBirthday(new \DateTime((new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c')));

        $em->persist($user);
        $em->flush();

        $todolists = $em->getRepository(Todolist::class)->findAll();
        $futureTodolist = $todolists[array_key_last($todolists)];

        // Update this user
        $url = $client->getContainer()->get('router')->generate('api_user_update', ['id' => $user->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastname(),
            'email' => $faker->email(),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'todolist' => $futureTodolist->getId()
        ]);
        $this->assertResponseIsSuccessful();

        $em->remove($user);
        $em->flush();
    }

    /**
     * Test the route api_user_update
     * User doesn't exists in database
     */
    public function testUserUpdateRouteUserDoesntExistsInDb()
    {
        $client = static::createClient();
        $faker = \Faker\Factory::create('fr_FR');

        // Update this user
        $url = $client->getContainer()->get('router')->generate('api_user_update', ['id' => 0], false);
        $crawler = $client->request('UPDATE', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastname(),
            'email' => $faker->email(),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
        ]);
        $this->assertResponseStatusCodeSame(404, 'User 0 does not exists');
    }

    /**
     * Test the route api_user_update
     * Todolist doesn't exists in database
     */
    public function testUserUpdateRouteTodolistDoesntExistsInDb()
    {
        $client = static::createClient();

        // Add new user
        $faker = \Faker\Factory::create('fr_FR');

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Create a user in the database
        $user = new User();
        $user = $user
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setEmail($faker->email)
            ->setPassword($faker->password(8, 40))
            ->setBirthday(new \DateTime((new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c')));

        $em->persist($user);
        $em->flush();

        // Update this user
        $url = $client->getContainer()->get('router')->generate('api_user_update', ['id' => $user->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastname(),
            'email' => $faker->email(),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'todolist' => 0
        ]);
        $this->assertResponseStatusCodeSame(500, 'Cannot fetch the 0 todolist id. It doesn\'t exists');

        $em->remove($user);
        $em->flush();
    }

    /**
     * Test the route api_user_update
     * Todolist data is not an int or a boolean
     */
    public function testUserUpdateRouteTodolistDataIsNotIntOrBool()
    {
        $client = static::createClient();

        // Add new user
        $faker = \Faker\Factory::create('fr_FR');

        // Get symfony services
        $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Create a user in the database
        $user = new User();
        $user = $user
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setEmail($faker->email)
            ->setPassword($faker->password(8, 40))
            ->setBirthday(new \DateTime((new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c')));

        $em->persist($user);
        $em->flush();

        // Update this user
        $url = $client->getContainer()->get('router')->generate('api_user_update', ['id' => $user->getId()], false);
        $crawler = $client->request('UPDATE', $url, [
            'firstname' => $faker->firstName(),
            'lastname' => $faker->lastname(),
            'email' => $faker->email(),
            'birthday' => (new \DateTime('now'))->sub(new \DateInterval('P13Y'))->format('c'),
            'todolist' => []
        ]);
        $this->assertResponseStatusCodeSame(500, 'Try to update a todolist with a non bool or integer value');

        $em->remove($user);
        $em->flush();
    }
}
