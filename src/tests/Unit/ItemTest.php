<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Item;
use App\Entity\Todolist;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    /**
     * Test initial values of the entity.
     */
    public function testInitialValues(): void
    {
        $item = new Item();
        $this->assertNull($item->getName());
        $this->assertNull($item->getContent());
        $this->assertNull($item->getCreatedAt());
        $this->assertNull($item->getTodolist());
    }

    /**
     * Test getter and setters + other (e.g. add, remove).
     */
    public function testAffectation(): void
    {
        $fakeData = [
            'name' => 'my first item',
            'content' => 'my super content',
            'todolist' => new Todolist(),
            'createdAt' => new \DateTime('now')
        ];

        $item = new Item();
        // Automatized affectation
        foreach ($fakeData as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $item = $item->$setter($value);
        }

        // Automatized retrivation
        foreach ($fakeData as $key => $value) {
            $getter = 'get' . ucfirst($key);
            $this->assertEquals($value, $item->$getter(), "The original $key property has change its value with the setter");
        }
    }
}
