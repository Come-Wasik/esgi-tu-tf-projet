# [ESGI - 4IW2]

![PHP](https://img.shields.io/badge/PHP-%5E7.4-blue) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/Come-Wasik/esgi-tu-tf-projet/master)

## A LIRE
La méthode `isValid()` de l'entité User a été changé en `checkValidity()` car il est plus pertinant de ne pas retourner un booléan valant vrai si au même endroit est mit en place un système d'exceptions. Il vaut mieux alors, n'utiliser que ce système d'exception.

La méthode `checkValidity()` renvoi une exception si une ou plusieurs erreurs sont trouvés, rien en d'autres cas.

## Description

This is a symfony project, about unitary test. This is for a school project at ESGI.
The web project is in the src directory. Then, as all symfony 5 projects:
+ Classes are in the directory `/src/Entity`
+ Services in `/src/Service`
+ Tests are in the directory `/tests`
  + Unitary tests: `/tests/unit`
  + Functional tests: `/tests/functional`
+ To insert fake data in database (fixture/factory): `/src/DataFixtures`

## Requirement

Docker

## Usage

### Starting website and tools
```bash
docker-compose up -d
docker-compose exec php bin/console d:s:u --force
docker-compose exec php bin/console d:f:l --append
docker-compose exec php bin/phpunit
```